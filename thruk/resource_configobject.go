package thruk

import (
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"gitlab.com/roviluca/thruk-go"
)

func resourceConfigObject() *schema.Resource {
	return &schema.Resource{
		Create: resourceConfigObjectCreate,
		Read:   resourceConfigObjectRead,
		Update: resourceConfigObjectUpdate,
		Delete: resourceConfigObjectDelete,

		Schema: map[string]*schema.Schema{
			"type": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"file": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"peer_key": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"read_only": &schema.Schema{
				Type:     schema.TypeInt,
				Optional: true,
			},
			"command_line": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"command_name": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"active_checks_enabled": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"check_freshness": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"check_interval": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"check_period": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"event_handler_enabled": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"failure_prediction_enabled": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"flap_detection_enabled": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"is_volatile": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"max_check_attempts": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"notification_interval": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"notification_options": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"notification_period": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"notifications_enabled": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"obsess_over_service": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"passive_checks_enabled": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"process_perf_data": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"register": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"retain_nonstatus_information": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"retain_status_information": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"retry_interval": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"alias": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"timeperiod_name": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"check_command": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"friday": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"monday": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"saturday": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"sunday": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"thursday": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"tuesday": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"wednesday": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"action_url": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"two_d_coords": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"three_d_coords": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"address": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"contact_groups": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"contacts": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"display_name": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"event_handler": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"first_notification_delay": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"flap_detection_options": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"freshness_threshold": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"high_flap_threshold": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"host_name": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"hostgroups": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"icon_image": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"icon_image_alt": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"initial_state": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"low_flap_threshold": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"notes": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"notes_url": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"obsess_over_host": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"parents": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"stalking_options": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"statusmap_image": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"use": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"vrml_image": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"worker": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"host_notification_commands": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"host_notification_options": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"host_notification_period": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"service_notification_commands": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"service_notification_options": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"service_notification_period": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"servicegroup_name": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"servicegroups": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"service_description": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
		},
	}
}

func generateConfigObjectFromResourceData(d *schema.ResourceData) thruk.ConfigObject {
	return thruk.ConfigObject{
		TYPE:                        d.Get("type").(string),
		FILE:                        d.Get("file").(string),
		Name:                        d.Get("name").(string),
		PEERKEY:                     d.Get("peer_key").(string),
		READONLY:                    d.Get("read_only").(int),
		CommandLine:                 d.Get("command_line").(string),
		CommandName:                 d.Get("command_name").(string),
		ActiveChecksEnabled:         d.Get("active_checks_enabled").(string),
		CheckFreshness:              d.Get("check_freshness").(string),
		CheckInterval:               d.Get("check_interval").(string),
		CheckPeriod:                 d.Get("check_period").(string),
		EventHandlerEnabled:         d.Get("event_handler_enabled").(string),
		FailurePredictionEnabled:    d.Get("failure_prediction_enabled").(string),
		FlapDetectionEnabled:        d.Get("flap_detection_enabled").(string),
		IsVolatile:                  d.Get("is_volatile").(string),
		MaxCheckAttempts:            d.Get("max_check_attempts").(string),
		NotificationInterval:        d.Get("notification_interval").(string),
		NotificationOptions:         getSliceOfStringsFromResourceData(d, "notification_options"),
		NotificationPeriod:          d.Get("notification_period").(string),
		NotificationsEnabled:        d.Get("notifications_enabled").(string),
		ObsessOverService:           d.Get("obsess_over_service").(string),
		PassiveChecksEnabled:        d.Get("passive_checks_enabled").(string),
		ProcessPerfData:             d.Get("process_perf_data").(string),
		Register:                    d.Get("register").(string),
		RetainNonstatusInformation:  d.Get("retain_nonstatus_information").(string),
		RetainStatusInformation:     d.Get("retain_status_information").(string),
		RetryInterval:               d.Get("retry_interval").(string),
		Alias:                       d.Get("alias").(string),
		TimeperiodName:              d.Get("timeperiod_name").(string),
		CheckCommand:                d.Get("check_command").(string),
		Friday:                      d.Get("friday").(string),
		Monday:                      d.Get("monday").(string),
		Saturday:                    d.Get("saturday").(string),
		Sunday:                      d.Get("sunday").(string),
		Thursday:                    d.Get("thursday").(string),
		Tuesday:                     d.Get("tuesday").(string),
		Wednesday:                   d.Get("wednesday").(string),
		ActionURL:                   d.Get("action_url").(string),
		TwoDCoords:                  d.Get("two_d_coords").(string),
		ThreeDCoords:                d.Get("three_d_coords").(string),
		Address:                     d.Get("address").(string),
		ContactGroups:               getSliceOfStringsFromResourceData(d, "contactgroups"),
		Contacts:                    getSliceOfStringsFromResourceData(d, "contacts"),
		DisplayName:                 d.Get("display_name").(string),
		EventHandler:                getSliceOfStringsFromResourceData(d, "event_handler"),
		FirstNotificationDelay:      d.Get("first_notification_delay").(string),
		FlapDetectionOptions:        getSliceOfStringsFromResourceData(d, "flap_detection_options"),
		FreshnessThreshold:          d.Get("freshness_threshold").(string),
		HighFlapThreshold:           d.Get("high_flap_threshold").(string),
		HostName:                    d.Get("host_name").(string),
		Hostgroups:                  getSliceOfStringsFromResourceData(d, "hostgroups"),
		IconImage:                   d.Get("icon_image").(string),
		IconImageAlt:                d.Get("icon_image_alt").(string),
		InitialState:                d.Get("initial_state").(string),
		LowFlapThreshold:            d.Get("low_flap_threshold").(string),
		Notes:                       d.Get("notes").(string),
		NotesURL:                    d.Get("notes_url").(string),
		ObsessOverHost:              d.Get("obsess_over_host").(string),
		Parents:                     getSliceOfStringsFromResourceData(d, "parents"),
		StalkingOptions:             getSliceOfStringsFromResourceData(d, "stalking_options"),
		StatusmapImage:              d.Get("statusmap_image").(string),
		Use:                         getSliceOfStringsFromResourceData(d, "use"),
		VrmlImage:                   d.Get("vrml_image").(string),
		WORKER:                      d.Get("worker").(string),
		HostNotificationCommands:    getSliceOfStringsFromResourceData(d, "host_notification_commands"),
		HostNotificationOptions:     getSliceOfStringsFromResourceData(d, "host_notification_options"),
		HostNotificationPeriod:      d.Get("host_notification_period").(string),
		ServiceNotificationCommands: getSliceOfStringsFromResourceData(d, "service_notification_commands"),
		ServiceNotificationOptions:  getSliceOfStringsFromResourceData(d, "service_notification_options"),
		ServiceNotificationPeriod:   d.Get("service_notification_period").(string),
		ServicegroupName:            d.Get("servicegroup_name").(string),
		Servicegroups:               getSliceOfStringsFromResourceData(d, "servicegroups"),
		ServiceDescription:          d.Get("service_description").(string),
	}
}

func resourceConfigObjectCreate(d *schema.ResourceData, m interface{}) error {
	apiClient := m.(*thrukWithLock)
	apiClient.Mux.Lock()
	defer apiClient.Mux.Unlock()

	item := generateConfigObjectFromResourceData(d)
	id, err := apiClient.Client.CreateConfigObject(item)
	if err != nil {
		return err
	}

	d.SetId(id)

	err2 := saveCheckReload(apiClient)
	if err2 != nil {
		return err2
	}
	return nil
}

func resourceConfigObjectRead(d *schema.ResourceData, m interface{}) error {
	apiClient := m.(*thrukWithLock)

	_, err := apiClient.Client.GetConfigObject(d.Id())
	if err == thruk.ErrorObjectNotFound {
		d.SetId("")
		return nil
	}
	return err
}

func resourceConfigObjectUpdate(d *schema.ResourceData, m interface{}) error {
	return resourceConfigObjectRead(d, m)
}

func resourceConfigObjectDelete(d *schema.ResourceData, m interface{}) error {
	apiClient := m.(*thrukWithLock)
	apiClient.Mux.Lock()
	defer apiClient.Mux.Unlock()

	err := apiClient.Client.DeleteConfigObject(d.Id())
	if err != nil {
		return err
	}

	d.SetId("")

	err2 := saveCheckReload(apiClient)
	if err2 != nil {
		return err2
	}

	return nil
}
