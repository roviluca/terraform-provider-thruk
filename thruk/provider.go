package thruk

import (
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"github.com/hashicorp/terraform-plugin-sdk/terraform"
	"gitlab.com/roviluca/thruk-go"
	"sync"
)

func Provider() terraform.ResourceProvider {
	return &schema.Provider{
		Schema: map[string]*schema.Schema{
			"url": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "Address of Thruk service",
			},
			"site_name": {
				Type:        schema.TypeString,
				Optional:    true,
				Default:     "demo",
				Description: "Thruk site name",
			},
			"username": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "Address of Thruk service",
			},
			"password": {
				Type:        schema.TypeString,
				Required:    true,
				Description: "Address of Thruk service",
				Sensitive:   true,
			},
			"skip_tls": {
				Type:        schema.TypeBool,
				Optional:    true,
				Default:     "false",
				Description: "Address of Thruk service",
			},
		},
		ResourcesMap: map[string]*schema.Resource{
			"thruk_config_object":    resourceConfigObject(),
			"thruk_cfg_host":         resourceHost(),
			"thruk_cfg_service":      resourceService(),
			"thruk_cfg_servicegroup": resourceServicegroup(),
			"thruk_cfg_command":      resourceCommand(),
		},
		ConfigureFunc: providerConfigure,
	}
}

func providerConfigure(d *schema.ResourceData) (interface{}, error) {
	url := d.Get("url").(string)
	site := d.Get("site_name").(string)
	username := d.Get("username").(string)
	password := d.Get("password").(string)
	skip_tls := d.Get("skip_tls").(bool)

	return &thrukWithLock{
		Client: thruk.NewThruk(url, site, username, password, skip_tls),
	}, nil
}

type thrukWithLock struct {
	Client *thruk.Thruk
	Mux    sync.Mutex
}
