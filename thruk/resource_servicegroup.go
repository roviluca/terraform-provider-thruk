package thruk

import (
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"gitlab.com/roviluca/thruk-go"
)

func resourceServicegroup() *schema.Resource {
	return &schema.Resource{
		Create: resourceServicegroupCreate,
		Read:   resourceServicegroupRead,
		Update: resourceServicegroupUpdate,
		Delete: resourceServicegroupDelete,

		Schema: map[string]*schema.Schema{
			"file": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"peer_key": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"read_only": &schema.Schema{
				Type:     schema.TypeInt,
				Optional: true,
			},
			"type": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"action_url": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"alias": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"members": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"notes": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"notes_url": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"register": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"servicegroup_members": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"servicegroup_name": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"use": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
		},
	}
}

func generateServicegroupFromResourceData(d *schema.ResourceData) thruk.Servicegroup {
	return thruk.Servicegroup{
		FILE:                d.Get("file").(string),
		READONLY:            d.Get("read_only").(int),
		TYPE:                d.Get("type").(string),
		ActionURL:           d.Get("action_url").(string),
		Alias:               d.Get("alias").(string),
		Members:             getSliceOfStringsFromResourceData(d, "members"),
		Name:                d.Get("name").(string),
		Notes:               d.Get("notes").(string),
		NotesURL:            d.Get("notes_url").(string),
		Register:            d.Get("register").(string),
		ServicegroupMembers: getSliceOfStringsFromResourceData(d, "servicegroup_members"),
		ServicegroupName:    d.Get("servicegroup_name").(string),
		Use:                 getSliceOfStringsFromResourceData(d, "use"),
	}

}

func resourceServicegroupCreate(d *schema.ResourceData, m interface{}) error {
	apiClient := m.(*thrukWithLock)
	apiClient.Mux.Lock()
	defer apiClient.Mux.Unlock()

	item := generateServicegroupFromResourceData(d)
	id, err := apiClient.Client.CreateServicegroup(item)
	if err != nil {
		return err
	}

	d.SetId(id)

	err2 := saveCheckReload(apiClient)
	if err2 != nil {
		return err2
	}
	return nil
}

func resourceServicegroupRead(d *schema.ResourceData, m interface{}) error {
	apiClient := m.(*thrukWithLock)

	servicegroup, err := apiClient.Client.GetServicegroup(d.Id())
	if err == thruk.ErrorObjectNotFound {
		d.SetId("")
		return nil
	}

	d.Set("file", getRelativePathWithoutLineNumber(servicegroup.FILE))

	d.Set("read_only", servicegroup.READONLY)
	d.Set("type", servicegroup.TYPE)
	d.Set("action_url", servicegroup.ActionURL)
	d.Set("alias", servicegroup.Alias)
	d.Set("members", servicegroup.Members)
	d.Set("name", servicegroup.Name)
	d.Set("notes", servicegroup.Notes)
	d.Set("notes_url", servicegroup.NotesURL)
	d.Set("register", servicegroup.Register)
	d.Set("servicegroup_members", servicegroup.ServicegroupMembers)
	d.Set("servicegroup_name", servicegroup.ServicegroupName)
	d.Set("use", servicegroup.Use)

	return err
}

func resourceServicegroupUpdate(d *schema.ResourceData, m interface{}) error {
	apiClient := m.(*thrukWithLock)
	apiClient.Mux.Lock()
	defer apiClient.Mux.Unlock()

	err := apiClient.Client.DeleteServicegroup(d.Id())
	if err != nil {
		return err
	}

	d.SetId("")

	item := generateServicegroupFromResourceData(d)
	id, err := apiClient.Client.CreateServicegroup(item)
	if err != nil {
		return err
	}

	d.SetId(id)

	err2 := saveCheckReload(apiClient)
	if err2 != nil {
		return err2
	}

	return resourceServicegroupRead(d, m)
}

func resourceServicegroupDelete(d *schema.ResourceData, m interface{}) error {
	apiClient := m.(*thrukWithLock)
	apiClient.Mux.Lock()
	defer apiClient.Mux.Unlock()

	err := apiClient.Client.DeleteServicegroup(d.Id())
	if err != nil {
		return err
	}

	d.SetId("")

	err2 := saveCheckReload(apiClient)
	if err2 != nil {
		return err2
	}

	return nil
}
