package thruk

import (
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"gitlab.com/roviluca/thruk-go"
)

func resourceService() *schema.Resource {
	return &schema.Resource{
		Create: resourceServiceCreate,
		Read:   resourceServiceRead,
		Update: resourceServiceUpdate,
		Delete: resourceServiceDelete,

		Schema: map[string]*schema.Schema{
			"file": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"read_only": &schema.Schema{
				Type:     schema.TypeInt,
				Optional: true,
			},
			"type": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"action_url": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"active_checks_enabled": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"check_command": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"check_freshness": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"check_interval": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"check_period": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"contact_groups": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"contacts": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"display_name": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"event_handler": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"event_handler_enabled": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"first_notification_delay": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"flap_detection_enabled": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"flap_detection_options": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"freshness_threshold": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"high_flap_threshold": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"host_name": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"hostgroup_name": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"icon_image": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"icon_image_alt": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"initial_state": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"is_volatile": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"low_flap_threshold": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"max_check_attempts": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"notes": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"notes_url": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"notification_interval": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"notification_options": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"notification_period": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"notifications_enabled": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"obsess_over_service": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"parents": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"passive_checks_enabled": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"process_perf_data": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"register": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"retain_nonstatus_information": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"retain_status_information": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"retry_interval": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"service_description": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"servicegroups": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"stalking_options": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"use": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"worker": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"failure_prediction_enabled": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
		},
	}
}

func generateServiceFromResourceData(d *schema.ResourceData) thruk.Service {
	return thruk.Service{
		FILE:                       d.Get("file").(string),
		READONLY:                   d.Get("read_only").(int),
		TYPE:                       d.Get("type").(string),
		ActionURL:                  d.Get("action_url").(string),
		ActiveChecksEnabled:        d.Get("active_checks_enabled").(string),
		CheckCommand:               d.Get("check_command").(string),
		CheckFreshness:             d.Get("check_freshness").(string),
		CheckInterval:              d.Get("check_interval").(string),
		CheckPeriod:                d.Get("check_period").(string),
		ContactGroups:              getSliceOfStringsFromResourceData(d, "contact_groups"),
		Contacts:                   getSliceOfStringsFromResourceData(d, "contacts"),
		DisplayName:                d.Get("display_name").(string),
		EventHandler:               getSliceOfStringsFromResourceData(d, "event_handler"),
		EventHandlerEnabled:        d.Get("event_handler_enabled").(string),
		FirstNotificationDelay:     d.Get("first_notification_delay").(string),
		FlapDetectionEnabled:       d.Get("flap_detection_enabled").(string),
		FlapDetectionOptions:       getSliceOfStringsFromResourceData(d, "flap_detection_options"),
		FreshnessThreshold:         d.Get("freshness_threshold").(string),
		HighFlapThreshold:          d.Get("high_flap_threshold").(string),
		HostName:                   getSliceOfStringsFromResourceData(d, "host_name"),
		HostgroupName:              getSliceOfStringsFromResourceData(d, "hostgroup_name"),
		IconImage:                  d.Get("icon_image").(string),
		IconImageAlt:               d.Get("icon_image_alt").(string),
		InitialState:               d.Get("initial_state").(string),
		IsVolatile:                 d.Get("is_volatile").(string),
		LowFlapThreshold:           d.Get("low_flap_threshold").(string),
		MaxCheckAttempts:           d.Get("max_check_attempts").(string),
		Name:                       d.Get("name").(string),
		Notes:                      d.Get("notes").(string),
		NotesURL:                   d.Get("notes_url").(string),
		NotificationInterval:       d.Get("notification_interval").(string),
		NotificationOptions:        getSliceOfStringsFromResourceData(d, "notification_options"),
		NotificationPeriod:         d.Get("notification_period").(string),
		NotificationsEnabled:       d.Get("notifications_enabled").(string),
		ObsessOverService:          d.Get("obsess_over_service").(string),
		Parents:                    getSliceOfStringsFromResourceData(d, "parents"),
		PassiveChecksEnabled:       d.Get("passive_checks_enabled").(string),
		ProcessPerfData:            d.Get("process_perf_data").(string),
		Register:                   d.Get("register").(string),
		RetainNonstatusInformation: d.Get("retain_nonstatus_information").(string),
		RetainStatusInformation:    d.Get("retain_status_information").(string),
		RetryInterval:              d.Get("retry_interval").(string),
		ServiceDescription:         d.Get("service_description").(string),
		Servicegroups:              getSliceOfStringsFromResourceData(d, "servicegroups"),
		StalkingOptions:            getSliceOfStringsFromResourceData(d, "stalking_options"),
		Use:                        getSliceOfStringsFromResourceData(d, "use"),
		WORKER:                     d.Get("worker").(string),
		FailurePredictionEnabled:   d.Get("failure_prediction_enabled").(string),
	}

}

func resourceServiceCreate(d *schema.ResourceData, m interface{}) error {
	apiClient := m.(*thrukWithLock)
	apiClient.Mux.Lock()
	defer apiClient.Mux.Unlock()

	item := generateServiceFromResourceData(d)
	id, err := apiClient.Client.CreateService(item)
	if err != nil {
		return err
	}

	d.SetId(id)

	err2 := saveCheckReload(apiClient)
	if err2 != nil {
		return err2
	}
	return nil
}

func resourceServiceRead(d *schema.ResourceData, m interface{}) error {
	apiClient := m.(*thrukWithLock)

	service, err := apiClient.Client.GetService(d.Id())
	if err == thruk.ErrorObjectNotFound {
		d.SetId("")
		return nil
	}

	d.Set("file", getRelativePathWithoutLineNumber(service.FILE))

	d.Set("read_only", service.READONLY)
	d.Set("type", service.TYPE)
	d.Set("action_url", service.ActionURL)
	d.Set("active_checks_enabled", service.ActiveChecksEnabled)
	d.Set("check_command", service.CheckCommand)
	d.Set("check_freshness", service.CheckFreshness)
	d.Set("check_interval", service.CheckInterval)
	d.Set("check_period", service.CheckPeriod)
	d.Set("contact_groups", service.ContactGroups)
	d.Set("contacts", service.Contacts)
	d.Set("display_name", service.DisplayName)
	d.Set("event_handler", service.EventHandler)
	d.Set("event_handler_enabled", service.EventHandlerEnabled)
	d.Set("first_notification_delay", service.FirstNotificationDelay)
	d.Set("flap_detection_enabled", service.FlapDetectionEnabled)
	d.Set("flap_detection_options", service.FlapDetectionOptions)
	d.Set("freshness_threshold", service.FreshnessThreshold)
	d.Set("high_flap_threshold", service.HighFlapThreshold)
	d.Set("host_name", service.HostName)
	d.Set("hostgroup_name", service.HostgroupName)
	d.Set("icon_image", service.IconImage)
	d.Set("icon_image_alt", service.IconImageAlt)
	d.Set("initial_state", service.InitialState)
	d.Set("is_volatile", service.IsVolatile)
	d.Set("low_flap_threshold", service.LowFlapThreshold)
	d.Set("max_check_attempts", service.MaxCheckAttempts)
	d.Set("name", service.Name)
	d.Set("notes", service.Notes)
	d.Set("notes_url", service.NotesURL)
	d.Set("notification_interval", service.NotificationInterval)
	d.Set("notification_options", service.NotificationOptions)
	d.Set("notification_period", service.NotificationPeriod)
	d.Set("notifications_enabled", service.NotificationsEnabled)
	d.Set("obsess_over_service", service.ObsessOverService)
	d.Set("parents", service.Parents)
	d.Set("passive_checks_enabled", service.PassiveChecksEnabled)
	d.Set("process_perf_data", service.ProcessPerfData)
	d.Set("register", service.Register)
	d.Set("retain_nonstatus_information", service.RetainNonstatusInformation)
	d.Set("retain_status_information", service.RetainStatusInformation)
	d.Set("retry_interval", service.RetryInterval)
	d.Set("service_description", service.ServiceDescription)
	d.Set("servicegroups", service.Servicegroups)
	d.Set("stalking_options", service.StalkingOptions)
	d.Set("use", service.Use)
	d.Set("worker", service.WORKER)
	d.Set("failure_prediction_enabled", service.FailurePredictionEnabled)

	return err
}

func resourceServiceUpdate(d *schema.ResourceData, m interface{}) error {
	apiClient := m.(*thrukWithLock)
	apiClient.Mux.Lock()
	defer apiClient.Mux.Unlock()

	err := apiClient.Client.DeleteService(d.Id())
	if err != nil {
		return err
	}

	d.SetId("")

	item := generateServiceFromResourceData(d)
	id, err := apiClient.Client.CreateService(item)
	if err != nil {
		return err
	}

	d.SetId(id)

	err2 := saveCheckReload(apiClient)
	if err2 != nil {
		return err2
	}

	return resourceServiceRead(d, m)
}

func resourceServiceDelete(d *schema.ResourceData, m interface{}) error {
	apiClient := m.(*thrukWithLock)
	apiClient.Mux.Lock()
	defer apiClient.Mux.Unlock()

	err := apiClient.Client.DeleteService(d.Id())
	if err != nil {
		return err
	}

	d.SetId("")

	err2 := saveCheckReload(apiClient)
	if err2 != nil {
		return err2
	}

	return nil
}
