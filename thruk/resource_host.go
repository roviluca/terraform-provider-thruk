package thruk

import (
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"gitlab.com/roviluca/thruk-go"
)

func resourceHost() *schema.Resource {
	return &schema.Resource{
		Create: resourceHostCreate,
		Read:   resourceHostRead,
		Update: resourceHostUpdate,
		Delete: resourceHostDelete,

		Schema: map[string]*schema.Schema{
			"file": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"read_only": &schema.Schema{
				Type:     schema.TypeInt,
				Optional: true,
			},
			"type": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"worker": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"active_checks_enabled": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"address": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"check_command": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"check_interval": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"check_period": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"event_handler_enabled": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"flap_detection_enabled": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"max_check_attempts": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"notification_interval": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"notification_options": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"notification_period": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"notifications_enabled": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"process_perf_data": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"register": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"retain_nonstatus_information": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"retain_status_information": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"retry_interval": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"action_url": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"failure_prediction_enabled": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"alias": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"use": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"two_d_coords": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"three_d_coords": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"check_freshness": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"contact_groups": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"contacts": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"display_name": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"first_notification_delay": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"flap_detection_options": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"freshness_threshold": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"high_flap_threshold": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"host_name": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"hostgroups": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"icon_image": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"icon_image_alt": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"initial_state": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"low_flap_threshold": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"notes": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"notes_url": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"obsess_over_host": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"parents": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"passive_checks_enabled": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"stalking_options": &schema.Schema{
				Type:     schema.TypeList,
				Optional: true,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
			},
			"statusmap_image": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"vrml_image": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
		},
	}
}

func generateHostFromResourceData(d *schema.ResourceData) thruk.Host {
	return thruk.Host{
		FILE:                       d.Get("file").(string),
		READONLY:                   d.Get("read_only").(int),
		TYPE:                       d.Get("type").(string),
		WORKER:                     d.Get("worker").(string),
		ActiveChecksEnabled:        d.Get("active_checks_enabled").(string),
		Address:                    d.Get("address").(string),
		CheckCommand:               d.Get("check_command").(string),
		CheckInterval:              d.Get("check_interval").(string),
		CheckPeriod:                d.Get("check_period").(string),
		EventHandlerEnabled:        d.Get("event_handler_enabled").(string),
		FlapDetectionEnabled:       d.Get("flap_detection_enabled").(string),
		MaxCheckAttempts:           d.Get("max_check_attempts").(string),
		Name:                       d.Get("name").(string),
		NotificationInterval:       d.Get("notification_interval").(string),
		NotificationOptions:        getSliceOfStringsFromResourceData(d, "notification_options"),
		NotificationPeriod:         d.Get("notification_period").(string),
		NotificationsEnabled:       d.Get("notifications_enabled").(string),
		PassiveChecksEnabled:       d.Get("passive_checks_enabled").(string),
		ProcessPerfData:            d.Get("process_perf_data").(string),
		Register:                   d.Get("register").(string),
		RetainNonstatusInformation: d.Get("retain_nonstatus_information").(string),
		RetainStatusInformation:    d.Get("retain_status_information").(string),
		RetryInterval:              d.Get("retry_interval").(string),
		ActionURL:                  d.Get("action_url").(string),
		FailurePredictionEnabled:   d.Get("failure_prediction_enabled").(string),
		Alias:                      d.Get("alias").(string),
		Use:                        getSliceOfStringsFromResourceData(d, "use"),
		TwoDCoords:                 d.Get("two_d_coords").(string),
		ThreeDCoords:               d.Get("three_d_coords").(string),
		ContactGroups:              getSliceOfStringsFromResourceData(d, "contact_groups"),
		Contacts:                   getSliceOfStringsFromResourceData(d, "contacts"),
		DisplayName:                d.Get("display_name").(string),
		FirstNotificationDelay:     d.Get("first_notification_delay").(string),
		FlapDetectionOptions:       getSliceOfStringsFromResourceData(d, "flap_detection_options"),
		CheckFreshness:             d.Get("check_freshness").(string),
		FreshnessThreshold:         d.Get("freshness_threshold").(string),
		HighFlapThreshold:          d.Get("high_flap_threshold").(string),
		HostName:                   d.Get("host_name").(string),
		Hostgroups:                 getSliceOfStringsFromResourceData(d, "hostgroups"),
		IconImage:                  d.Get("icon_image").(string),
		IconImageAlt:               d.Get("icon_image_alt").(string),
		InitialState:               d.Get("initial_state").(string),
		LowFlapThreshold:           d.Get("low_flap_threshold").(string),
		Notes:                      d.Get("notes").(string),
		NotesURL:                   d.Get("notes_url").(string),
		ObsessOverHost:             d.Get("obsess_over_host").(string),
		Parents:                    getSliceOfStringsFromResourceData(d, "parents"),
		StalkingOptions:            getSliceOfStringsFromResourceData(d, "stalking_options"),
		StatusmapImage:             d.Get("statusmap_image").(string),
		VrmlImage:                  d.Get("vrml_image").(string),
	}

}

func resourceHostCreate(d *schema.ResourceData, m interface{}) error {
	apiClient := m.(*thrukWithLock)
	apiClient.Mux.Lock()
	defer apiClient.Mux.Unlock()

	item := generateHostFromResourceData(d)
	id, err := apiClient.Client.CreateHost(item)
	if err != nil {
		return err
	}

	d.SetId(id)

	err2 := saveCheckReload(apiClient)
	if err2 != nil {
		return err2
	}
	return nil
}

func resourceHostRead(d *schema.ResourceData, m interface{}) error {
	apiClient := m.(*thrukWithLock)

	host, err := apiClient.Client.GetHost(d.Id())
	if err == thruk.ErrorObjectNotFound {
		d.SetId("")
		return nil
	}

	d.Set("file", getRelativePathWithoutLineNumber(host.FILE))

	d.Set("id", host.ID)
	d.Set("read_only", host.READONLY)
	d.Set("type", host.TYPE)
	d.Set("worker", host.WORKER)
	d.Set("active_checks_enabled", host.ActiveChecksEnabled)
	d.Set("address", host.Address)
	d.Set("check_command", host.CheckCommand)
	d.Set("check_interval", host.CheckInterval)
	d.Set("check_period", host.CheckPeriod)
	d.Set("event_handler_enabled", host.EventHandlerEnabled)
	d.Set("flap_detection_enabled", host.FlapDetectionEnabled)
	d.Set("max_check_attempts", host.MaxCheckAttempts)
	d.Set("name", host.Name)
	d.Set("notification_interval", host.NotificationInterval)
	d.Set("notification_options", host.NotificationOptions)
	d.Set("notification_period", host.NotificationPeriod)
	d.Set("notifications_enabled", host.NotificationsEnabled)
	d.Set("process_perf_data", host.ProcessPerfData)
	d.Set("register", host.Register)
	d.Set("retain_nonstatus_information", host.RetainNonstatusInformation)
	d.Set("retain_status_information", host.RetainStatusInformation)
	d.Set("retry_interval", host.RetryInterval)
	d.Set("action_url", host.ActionURL)
	d.Set("failure_prediction_enabled", host.FailurePredictionEnabled)
	d.Set("alias", host.Alias)
	d.Set("use", host.Use)
	d.Set("two_d_coords", host.TwoDCoords)
	d.Set("three_d_coords", host.ThreeDCoords)
	d.Set("check_freshness", host.CheckFreshness)
	d.Set("contact_groups", host.ContactGroups)
	d.Set("contacts", host.Contacts)
	d.Set("display_name", host.DisplayName)
	d.Set("first_notification_delay", host.FirstNotificationDelay)
	d.Set("flap_detection_options", host.FlapDetectionOptions)
	d.Set("freshness_threshold", host.FreshnessThreshold)
	d.Set("high_flap_threshold", host.HighFlapThreshold)
	d.Set("host_name", host.HostName)
	d.Set("hostgroups", host.Hostgroups)
	d.Set("icon_image", host.IconImage)
	d.Set("icon_image_alt", host.IconImageAlt)
	d.Set("initial_state", host.InitialState)
	d.Set("low_flap_threshold", host.LowFlapThreshold)
	d.Set("notes", host.Notes)
	d.Set("notes_url", host.NotesURL)
	d.Set("obsess_over_host", host.ObsessOverHost)
	d.Set("parents", host.Parents)
	d.Set("passive_checks_enabled", host.PassiveChecksEnabled)
	d.Set("stalking_options", host.StalkingOptions)
	d.Set("statusmap_image", host.StatusmapImage)
	d.Set("vrml_image", host.VrmlImage)

	return err
}

func resourceHostUpdate(d *schema.ResourceData, m interface{}) error {
	apiClient := m.(*thrukWithLock)
	apiClient.Mux.Lock()
	defer apiClient.Mux.Unlock()

	err := apiClient.Client.DeleteHost(d.Id())
	if err != nil {
		return err
	}

	d.SetId("")

	item := generateHostFromResourceData(d)
	id, err := apiClient.Client.CreateHost(item)
	if err != nil {
		return err
	}

	d.SetId(id)

	err2 := saveCheckReload(apiClient)
	if err2 != nil {
		return err2
	}

	return resourceHostRead(d, m)
}

func resourceHostDelete(d *schema.ResourceData, m interface{}) error {
	apiClient := m.(*thrukWithLock)
	apiClient.Mux.Lock()
	defer apiClient.Mux.Unlock()

	err := apiClient.Client.DeleteHost(d.Id())
	if err != nil {
		return err
	}

	d.SetId("")

	err2 := saveCheckReload(apiClient)
	if err2 != nil {
		return err2
	}

	return nil
}
