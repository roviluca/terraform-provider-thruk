package thruk

import (
	"errors"
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"regexp"
)

func getSliceOfStringsFromResourceData(d *schema.ResourceData, key string) []string {
	keyValue := d.Get(key)
	if keyValue == nil {
		return []string{}
	}
	itemsRaw := d.Get(key).([]interface{})
	items := make([]string, len(itemsRaw))
	for i, raw := range itemsRaw {
		items[i] = raw.(string)
	}
	return items
}

func getRelativePathWithoutLineNumber(objectPath string) string {
	getRelativePath := regexp.MustCompile(`^(\/[A-Za-z\.-]+){6}\/`)
	path := getRelativePath.ReplaceAllString(objectPath, "")

	removePositionInFile := regexp.MustCompile(`:[0-9]*$`)
	path = removePositionInFile.ReplaceAllString(path, "")
	return path
}

func saveCheckReload(apiClient *thrukWithLock) error {
	if err := apiClient.Client.SaveConfigs(); err != nil {
		return err
	}
	if ok := apiClient.Client.CheckConfig(); !ok {
		return errors.New("check of config failed")
	}
	return nil
}
