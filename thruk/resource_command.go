package thruk

import (
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"gitlab.com/roviluca/thruk-go"
)

func resourceCommand() *schema.Resource {
	return &schema.Resource{
		Create: resourceCommandCreate,
		Read:   resourceCommandRead,
		Update: resourceCommandUpdate,
		Delete: resourceCommandDelete,

		Schema: map[string]*schema.Schema{
			"file": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"peer_key": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"read_only": &schema.Schema{
				Type:     schema.TypeInt,
				Optional: true,
			},
			"type": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"command_line": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"command_name": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
			"register": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
			},
		},
	}
}

func generateCommandFromResourceData(d *schema.ResourceData) thruk.Command {
	return thruk.Command{
		FILE:        d.Get("file").(string),
		READONLY:    d.Get("read_only").(int),
		TYPE:        d.Get("type").(string),
		CommandLine: d.Get("command_line").(string),
		CommandName: d.Get("command_name").(string),
		Name:        d.Get("name").(string),
		Register:    d.Get("register").(string),
	}

}

func resourceCommandCreate(d *schema.ResourceData, m interface{}) error {
	apiClient := m.(*thrukWithLock)
	apiClient.Mux.Lock()
	defer apiClient.Mux.Unlock()

	item := generateCommandFromResourceData(d)
	id, err := apiClient.Client.CreateCommand(item)
	if err != nil {
		return err
	}

	d.SetId(id)

	err2 := saveCheckReload(apiClient)
	if err2 != nil {
		return err2
	}
	return nil
}

func resourceCommandRead(d *schema.ResourceData, m interface{}) error {
	apiClient := m.(*thrukWithLock)

	command, err := apiClient.Client.GetCommand(d.Id())
	if err == thruk.ErrorObjectNotFound {
		d.SetId("")
		return nil
	}

	d.Set("file", getRelativePathWithoutLineNumber(command.FILE))

	d.Set("read_only", command.READONLY)
	d.Set("type", command.TYPE)
	d.Set("command_line", command.CommandLine)
	d.Set("command_name", command.CommandName)
	d.Set("name", command.CommandName)
	d.Set("register", command.Register)

	return err
}

func resourceCommandUpdate(d *schema.ResourceData, m interface{}) error {
	apiClient := m.(*thrukWithLock)
	apiClient.Mux.Lock()
	defer apiClient.Mux.Unlock()

	err := apiClient.Client.DeleteCommand(d.Id())
	if err != nil {
		return err
	}

	d.SetId("")

	item := generateCommandFromResourceData(d)
	id, err := apiClient.Client.CreateCommand(item)
	if err != nil {
		return err
	}

	d.SetId(id)

	err2 := saveCheckReload(apiClient)
	if err2 != nil {
		return err2
	}

	return resourceCommandRead(d, m)
}

func resourceCommandDelete(d *schema.ResourceData, m interface{}) error {
	apiClient := m.(*thrukWithLock)
	apiClient.Mux.Lock()
	defer apiClient.Mux.Unlock()

	err := apiClient.Client.DeleteCommand(d.Id())
	if err != nil {
		return err
	}

	d.SetId("")

	err2 := saveCheckReload(apiClient)
	if err2 != nil {
		return err2
	}

	return nil
}
