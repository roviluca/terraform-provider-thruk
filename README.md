# terraform-provider-thruk

## Built for
https://www.terraform.io
<img src="https://cdn.rawgit.com/hashicorp/terraform-website/master/content/source/assets/images/logo-hashicorp.svg" width="600px">

## Requirements

-	[Terraform](https://www.terraform.io/downloads.html) 0.12.x
-	[Go](https://golang.org/doc/install) 1.13.x (to build the provider plugin)

## Building The Provider

Clone repository to: `$GOPATH/src/gitlab.com/roviluca/terraform-provider-thruk`

```sh
$ mkdir -p $GOPATH/src/gitlab.com/roviluca; cd $GOPATH/src/gitlab.com/roviluca
$ git clone https://gitlab.com/roviluca/terraform-provider-thruk.git
```

#### `go build` and install it local to your changes

If you would rather install the provider locally and not impact the
stable version you already have installed, you can use the
`~/.terraformrc` file to tell Terraform where your provider is. You do
this by building the provider using Go.

```sh
$ cd $GOPATH/src/gitlab.com/roviluca/terraform-provider-thruk
$ go build -o terraform-provider-thruk
```

And then update your `~/.terraformrc` file to point at the location
you've built it.

```
providers {
  thruk = "${GOPATH}/src/gitlab.com/roviluca/terraform-provider-thruk/terraform-provider-thruk"
}
```

A caveat with this approach is that you will need to run `terraform
init` whenever the provider is rebuilt. You'll also need to remember to
comment it/remove it when it's not in use to avoid tripping yourself up.

## Developing the Provider

If you wish to work on the provider, you'll first need [Go](http://www.golang.org)
installed on your machine (version 1.11+ is *required*). You'll also need to
correctly setup a [GOPATH](http://golang.org/doc/code.html#GOPATH), as well
as adding `$GOPATH/bin` to your `$PATH`.

See above for which option suits your workflow for building the provider.

**Tests has not been written yet. **


## Managing dependencies

Terraform providers use [Go modules][go modules] to manage the
dependencies. To add or update a dependency, you would run the
following (`v1.2.3` of `foo` is a new package we want to add):

```
# Depending on your environment, you may need to `export GO111MODULE=on`
# before using these commands.

$ go get foo@v1.2.3
$ go mod tidy
$ go mod vendor
```

Stepping through the above commands:

- `go get foo@v1.2.3` fetches version `v1.2.3` from the source (if
    needed) and adds it to the `go.mod` file for use.
- `go mod tidy` cleans up any dangling dependencies or references that
  aren't defined in your module file.
- `go mod vendor` manages the `vendor` directory of the project. This is
  done to maintain backwards compatibility with older versions of Go
  that don't support Go modules.

(The example above will also work if you'd like to upgrade to `v1.2.3`)

If you wish to remove a dependency, you can remove the reference from
`go.mod` and use the same commands above but omit the initial `go get`.

[go modules]: https://github.com/golang/go/wiki/Modules
